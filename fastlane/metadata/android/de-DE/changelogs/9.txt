* Fehlerbehebungen
* DevTools
* Aktualisierte Blockierlisten
* Aktualisierter SaveDialog
* Monocles-Suchinstanz auf monocles.de geändert
* Aktualisierte Strings
* WebViews Dunkelmodus korrigiert
* Nachtmodus aktualisieren
