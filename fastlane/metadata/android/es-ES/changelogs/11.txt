• Corregir una condición de carrera que a veces provocaba que la pestaña actual no fuera la pestaña activa.
• Resaltar el fondo de las configuraciones de dominio que no son por defecto.
• Eliminar los términos lista blanca y lista negra del proyecto.
• Normalizar los nombres de archivo sugeridos.
• Actualizar en la barra de aplicaciones el color de fondo del proxy.
• Restaurar la posición de desplazamiento al reiniciar la configuración de la app.
• Migrar el último código a Kotlin y eliminar el obsoleto AsyncTasks.
• Optimizar varios trozos del código para que funcione de forma más eficiente.
• Reactivar el tema oscuro de WebView en Android 7-9 (API 24-28).
• Añadir una acción de cancelación a la snackbar Guardar URL.
• Cambiar los interruptores de configuración de dominio a listas desplegables que incluyan “predeterminado del sistema”.
• Permitir duplicar los nombres de carpeta de marcadores.
• Corregir un fallo si se reinicia Navegador monocles mientras el cuadro de diálogo de desajuste de anclajes es mostrado.
• Modernizar el código del adaptador del paginador de WebView.
• Corregir un fallo al mover un marcador a la carpeta de inicio.
• Aprovechar la funcionalidad secreta e indocumentada de Webview: Ver Fuente.
• Arreglar la Configuración de Dominio que a veces se crea como Habilitada en vez de Predeterminada del sistema.
• Mover el Agente de Usuario debajo de Almacenamiento DOM en la lista de configuración de dominio.
• Crear un diálogo especial de cifrado para las URLs de contenido.
• Corregir un fallo si Navegador monocles se reinicia mientras el diálogo de error de certificado SSL es mostrado.
• Corregir un retraso si Navegador monocles se reinicia mientras el diálogo de autenticación HTTP es mostrado.
• Actualizada la traducción al español proporcionada por Jose A. León.
