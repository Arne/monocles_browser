• Colorazione dello sfondo delle impostazioni dei domini diverse dalle impostazioni di default.
• Eliminazione dei termini whitelist e blacklist dal progetto.
• Standardizzazione dei nomi dei file suggeriti.
• Aggiornamento nella barra dell'app del colore di sfondo del proxy.
• Ripristino della posizione dello scrolling al riavvio delle impostazioni dell'app.
• Migrazione dell'ultima porzione di codice in Kotlin e rimozione delle deprecate AsyncTasks.
• Ottimizzazione di diverse porzioni del codice per un'esecuzione più efficiente.
• Abilitato nuovamente il tema scuro di WebView su Android 7-9 (API 24-28).
• Aggiunta l'azione di annullamento nella snackbar per il salvataggio delle URL.
• Modifica degli interruttori delle impostazioni dei domini in menù a tendina che includono anche il "default di sistema".
• Aggiunta la possibilità di duplicare i nomi delle cartelle dei segnalibri.
• Correzione di un crash che avveniva quando monocles Browser veniva riavviato mentre era visualizzata la finestra di dialogo di disallineamento dei certificati appuntati.
• Modernizzazione del codice dell'adattatore delle pagine di WebView.
• Sistemazione di un crash che avveniva muovendo un segnalibro nella cartella principale.
• Utilizzo della segreta e non documentata funzionalità di visione della sorgente di WebView.
• Fix Domain Settings sometimes being created as Enabled instead of System Default.
• Spostamento dello User Agent al di sotto del DOM Storage nell'elenco delle impostazioni dei domini.
• Creazione di una speciale finestra di dialogo di cifratura per gli URL di contenuto.
• Sistemazione di un crash che avveniva quando monocles Browser era riavviato mentre era visualizzata la finestra di dialogo di errore del certificato SSL.
• Sistemazione di un rallentamento che avveniva quando monocles Browser era riavviato mentre era visualizzata la finestra di dialogo di autenticazione HTTP.
• Aggiornamento della traduzione Italiana fornita da Francesco Buratti.
