* Correções de erros
* DevTools
* Listas de bloqueio atualizadas
* Atualizado SaveDialog
* Mudou a instância de busca de monóculos para monocles.de
* Cordas atualizadas
* Fixar o modo escuro do WebView
* Atualizar modo noturno
