* Prevent the wrong page being displayed on restart
* Update to Android 14
* Update the URL syntax highlighter for `view-source:`
* Always close the drawers when opening a new intent.
* Display SSL information in View Headers. 
* Add a scroll to top/bottom navigation view entry.
* A more elegant fix for spurious unencrypted website dialogs.
* Add options to copy, share, and save View Headers.
* Change monocles search instance
* Add options to copy, share, and save View Headers.
* Add share entries to the WebView context menus.
* Add a flow layout to the Headers SSL buttons
* Create new domain settings with the currently applied settings.
* Make About > Version display the full WebView version again.
* Replace `Loading...` and favorite icon on tab after download dialog opens. 
* Open new tabs adjacent to the current tab.
* Add import and export of bookmarks to HTML file.
* Add an option to display under the camera cutouts. 
* Update favorite icons when navigating history. 
* Attempt to handle race conditions with the creation of tabs.
* Use the website title as the default file name when saving .mht archives.
* Reimplement ActionBarDrawerToggle to get rid of the wasted padding. 
* Move the progress bar to the bottom when using the bottom app bar.
* Consider shrinking the progress bar from 3dp to 2dp.
* Change the default for scrolling the app bar to false.
